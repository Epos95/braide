# Brainfuck IDE

## WHY
One of the main difficulties of writing a *proper* brainfuck program is the limitation when it comes to abstraction.
Brainfuck has NO way of abstracting over data (through datastructures) or program structure (through functions) this programs aim is to atleast mitigate these issues when writing serious brainfuck programs.

## HOW
It should acomplish this goal by: 
* keeping track of data for the programer 
* checking for code repetition and establish make shift functions
* proper memory abstraction (write protected memory and proper memory division)
* some other things i cant think of right now

## WHY 2.0
Esolangs are beautiful and deserve recognition as their own respective pieces of art but i personally believe brainfuck can be used as a "game" like introduction to low level programming and manual memory management.
The more lofty goal of this program is to further both these purposes.

