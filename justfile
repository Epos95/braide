# The default case for printing all the just commands
default:
	@just --list

# Generate and open docs 
doc:
	@cargo doc --no-deps --open --bins

# Run the application and reset terminal on crash :D
run:
	@cargo b
	@cargo r 2> LOG.txt || stty sane
	@cat LOG.txt | less
