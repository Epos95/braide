use crate::EditorPopup;
use crate::Interpretation;
use crate::OpenTab;

use crate::Editor;

use tui::{backend::Backend, layout::Rect, Frame};

/// Big struct for describing the entire application and collecting all the available [`OpenTab`](crate::open_tab::OpenTab)'s.
pub struct App {
    /// `titles`: Storage of the names of all available [`OpenTab`](crate::open_tab::OpenTab)'s.
    pub titles: Vec<String>,

    /// `tab_index`: A index describing which tab is currently open, necesary (so far) for highlighting in the main list of tab titles.
    pub tab_index: usize,

    /// [`editor`](crate::editor::Editor): Structure handling everything to do with editing.
    pub editor: Editor,

    /// [`interpreter`](crate::interpreter::Interpretation): Structure handling the interpretation of the edited code.
    pub interpreter: Interpretation,

    /// [`open_tab`](crate::open_tab::OpenTab): For keeping track of which tab is currently open.
    pub open_tab: OpenTab,

    /// `current_message`: The string that gets show as the status message in the lowest part of the TUI.
    pub current_message: String,

    /// Place to store the input from the user
    pub input_buffer: String,

    /// How many iterations are allowed.
    /// Meant to fix infinite loops.
    pub iteration_limit: u64
}

impl App {
    /// Creates a new `App` with the contents of the given file.
    ///
    /// Panics on a invalid filename!
    pub fn new(filename: String) -> App {
        // Try and create a editor from the given file, if not we panic
        // in the future we might want to open a popup to perhaps open
        // another file instead.
        let editor: Editor = Editor::new(filename).expect("Invalid filename!");
        let interpreter = Interpretation::from(editor.editor_friendly_content());

        App {
            // We could rewrite this (titles) by iterating over `OpenTab`'s variants
            // but I am very lazy and this requires to import some library.
            titles: vec!["Editor", "Run"]
                .iter()
                .map(|x| x.to_string())
                .collect::<Vec<String>>(),
            tab_index: 0,
            interpreter,
            editor,
            open_tab: OpenTab::Editor,
            current_message: String::from("Welcome to Braide!"),
            input_buffer: String::new(),
            iteration_limit: 696969,
        }
    }

    /// Updates the cursor based on what window the `App` is currently showing.
    ///
    /// Is only meant to be called from the module responsible for rendering!
    pub fn cursor<B: Backend>(&self, f: &mut Frame<B>, chunks: Vec<Rect>) {
        match self.open_tab {
            OpenTab::Editor => match self.editor.current_popup {
                EditorPopup::Variables => {
                    f.set_cursor(
                        chunks[0].x + self.editor.cursor_handler.x() as u16,
                        chunks[0].y + self.editor.cursor_handler.y() as u16,
                    );
                }
                EditorPopup::None => {
                    f.set_cursor(
                        chunks[1].x + self.editor.cursor_handler.x() as u16 + 1,
                        chunks[1].y + self.editor.cursor_handler.y() as u16 + 1,
                    );
                }
            },
            OpenTab::Run => {
                //
            }
        }
    }
}
