#[derive(Clone, Copy, Debug)]
pub struct Cursor(pub usize, pub usize);

impl Cursor {

}

#[derive(Clone)]
pub struct CursorHandler {
    cursors: Vec<Cursor>,
}

impl CursorHandler {
    pub fn new(x: usize, y: usize) -> CursorHandler {
        CursorHandler {
            cursors: vec![Cursor(x, y)],
        }
    }

    pub fn x(&self) -> usize {
        self.cursors.last().unwrap().0
    }

    pub fn y(&self) -> usize {
        self.cursors.last().unwrap().1
    }

    pub fn incr_x(&mut self, value: i32) {
        let len = self.cursors.len();

        if value < 0 && value.abs() > self.cursors[len - 1].0 as i32 {
            panic!(
                "Tried to increment cursor.x ({}) by {}",
                self.cursors[len - 1].0,
                value
            );
        } else if value < 0 {
            self.cursors[len - 1].0 -= value.abs() as usize;
        } else {
            self.cursors[len - 1].0 += value.abs() as usize;
        }
    }

    pub fn incr_y(&mut self, value: i32) {
        let len = self.cursors.len();

        if value < 0 && value.abs() > self.cursors[len - 1].1 as i32 {
            panic!(
                "Tried to increment cursor.y ({}) by {}",
                self.cursors[len - 1].1,
                value
            );
        } else if value < 0 {
            self.cursors[len - 1].1 -= value.abs() as usize;
        } else {
            self.cursors[len - 1].1 += value.abs() as usize;
        }
    }

    pub fn add(&mut self, x: usize, y: usize) {
        self.cursors.push(Cursor(x, y));
    }

    pub fn pop(&mut self) {
        // bounds check so we cant pop the last cursor
        if self.cursors.len() > 1 {
            self.cursors.pop();
        }
    }

    pub fn set_x(&mut self, value: usize) {
        let len = self.cursors.len();
        self.cursors[len - 1].0 = value;
    }

    pub fn set_y(&mut self, value: usize) {
        let len = self.cursors.len();
        self.cursors[len - 1].1 = value;
    }

    pub fn get(&self, i: usize) -> Cursor {
        *self.cursors.get(i).unwrap()
    }
}

#[cfg(test)]
pub mod cursor_handler_tests {
    use super::*;

    #[test]
    fn creation_test() {
        let _c = CursorHandler::new(0, 2);
    }

    #[test]
    fn getting_test() {
        let c = CursorHandler::new(0, 2);
        assert_eq!(c.x(), 0);
        assert_eq!(c.y(), 2);
    }

    #[test]
    fn incr_pos_test() {
        let mut c = CursorHandler::new(2, 0);
        c.incr_x(1);
        c.incr_y(4);
        assert_eq!(c.x(), 3);
        assert_eq!(c.y(), 4);
    }

    #[test]
    #[should_panic]
    fn incr_neg_test() {
        let mut c = CursorHandler::new(2, 0);
        c.incr_x(1);
        c.incr_y(-4);

        assert_eq!(c.y(), 0);
    }

    #[test]
    fn more_cursors_one() {
        let mut c = CursorHandler::new(2, 0);

        // Go two chars to right
        c.incr_x(2);
        assert_eq!(c.x(), 4);
        // Go three rows down
        c.incr_y(3);

        // Push new cursor
        c.add(0, 0);

        c.incr_x(9);
        assert_eq!(c.x(), 9);

        c.pop();

        assert_eq!(c.x(), 4);
        assert_eq!(c.y(), 3);
    }

    #[test]
    fn neg_y() {
        let mut c = CursorHandler::new(0, 0);
        c.incr_y(2);
        assert_eq!(c.y(), 2);
        c.incr_y(-1);
    }

    #[test]
    fn neg_x() {
        let mut c = CursorHandler::new(0, 0);
        c.incr_x(2);
        assert_eq!(c.x(), 2);
        c.incr_x(-1);
    }

    #[test]
    #[should_panic]
    fn neg_panic_y() {
        let mut c = CursorHandler::new(0, 0);
        c.incr_y(-2);
    }

    #[test]
    #[should_panic]
    fn neg_panic_x() {
        let mut c = CursorHandler::new(0, 0);
        c.incr_x(-2);
    }
}
