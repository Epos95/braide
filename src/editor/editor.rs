use braide::debug;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Write;

use braide::Direction as BraideDirection;

use super::cursor_handler::CursorHandler;
use super::variable_popup::Variable;
use super::variable_popup::VariableList;
use super::variable_popup::VariablePopup;

/// Describes the editor where the user writes code.
/// Represents rows of code as a `Vec<String>`.
pub struct Editor {
    /// The filename to save/read the file under.
    filename: String,

    /// The content of the file as a vector where each line (separated by `\n`) is a item in the vector.
    // TODO: Rewamp this to not represent each line as a String and instead use a custom struct
    //       This will give us better control over pop() and stuff
    //       We could also take it a bit farther and let the line numbers be their own variable which
    //       can be prepended to the content before returning the thing to actually print
    //       This WILL work if we also rewamp the entire ui system
    //       for that we would need the line numbers to be in their own separate Paragraph struct
    //       though what happens if we have increasing line numbers?
    content: Vec<String>,

    /// Structure handling the cursors for all popups and things.
    pub cursor_handler: CursorHandler,

    /// The Variable popup that *could* be showing right now.
    pub variable_popup: VariablePopup,

    /// The currently open popup, see [`EditorPopup`](crate::editor::EditorPopup) for more.
    pub current_popup: EditorPopup,

    /// How far into memory we will store the variables.
    memory_offset: u16,

    pub scroll: u16,
    pub screen_height: u16,
    pub screen_width: u16,
    pub rel_cursor_y: u16,
}

/// Any kind of popup that can appear in the Editor tab.
pub enum EditorPopup {
    /// * `None`:      No popup is showing right now
    None,

    /// * `Variables`: The variables window is currently showing.
    Variables,
}

impl Editor {
    /// New method for `Editor`, fails when given a invalid filename (or just one it cant find.)
    pub fn new(filename: String) -> Option<Self> {
        let content = BufReader::new(File::open(&filename).ok()?)
            .lines()
            .filter(|x| (*x).is_ok())
            .map(|x| x.unwrap())
            .collect::<Vec<String>>();

        let len = content.len().to_string().len() + 1;

        Some(Editor {
            filename,
            content,
            variable_popup: VariablePopup {
                text_buffer: String::new(),
                variables: VariableList::with_items(vec![]),
                highlighted_item: 0,
            },
            cursor_handler: CursorHandler::new(len, 0),
            current_popup: EditorPopup::None,
            memory_offset: 500,
            scroll: 0,
            screen_height: u16::MAX, // placeholder!
            screen_width: u16::MAX,  // also kind of placeholder.
            rel_cursor_y: 0,
        })
    }

    fn get_left_offset(&self) -> usize {
        self.content.len().to_string().len() + 1
    }

    /// Returns the contents of the file as a `String` formatted for printing in the edtior
    /// Also updates the line numbers inside it, this *seems* bad but idk how else to have
    /// left_offset get consicestnly updated.
    pub fn editor_friendly_content(&self) -> String {
        // this needs someway to introduce *more* whitespace when printing numbers 0-9
        // so basically:
        // 100-999 should result in: 100 LINE
        // 10-99   should result in: 10  LINE
        // 1-9     should result in: 9   LINE
        //                          |    |
        //                          \    /
        //                           \  /
        //                            \/
        //                    self.get_left_offset()
        // (which is equal to the length of the number of items in array)

        self.content
            .iter()
            .enumerate()
            .map(|(i, line)| {

                let instructions = vec!['+', '[', '>', '<', ']', '-', ',', '.'];

                let c = line.chars().next().unwrap_or_default();
                let line = if instructions.contains(&c) && line.chars().all(|x| x == c) && line.len() > 5 {
                    format!("{c}x{}", line.len())
                } else {
                    line.to_string()
                };

                let n_whitespaces = (i + 1).to_string().len();
                format!(
                    "{}{}{}",
                    i + 1,
                    " ".repeat(self.get_left_offset() - n_whitespaces),
                    line
                )
            })
            .collect::<Vec<String>>()
            .join("\n")
    }

    /// Returns the contents of the file as a `Vec<String>`.
    pub fn content(&self) -> Vec<String> {
        self.content.clone()
    }

    /// Insert a character at the current cursor position.
    /// Behaves differently for each variant of `EditorPopup`
    pub fn insert_at_cursor(&mut self, c: char) {
        match &mut self.current_popup {
            EditorPopup::None => {
                let left_offset = self.get_left_offset();

                // This basically works but HOOO BOY is it bad!
                let len_rest = self.content[self.cursor_handler.y()]
                    .clone()
                    .split_off(
                        self.cursor_handler
                            .x()
                            .clamp(0, self.content[self.cursor_handler.y()].len()),
                    )
                    .len();

                // this doesnt work, has a offset for some reason... 
                // Dont really know why but ehhh
                // Maybe because of rel_cursor!

                // IMPORTANT: It would make sense that rel_cursor needs to be added to cursor_handler...

                if self.cursor_handler.x() + left_offset + 2 + len_rest < self.screen_width as usize {
                    self.content[self.cursor_handler.y()]
                        .insert(self.cursor_handler.x() - left_offset, c);
                    self.cursor_handler.incr_x(1);
                } else {
                    debug(format!("should insert char: {}, len_rest: {len_rest}\nscreenwidth: {}\nthing: {}", self.cursor_handler.x() + left_offset + 2 + len_rest < self.screen_width as usize, self.screen_width, self.cursor_handler.x() + left_offset + 2 + len_rest));
                }
            }
            EditorPopup::Variables => {
                // This lets us filter out specific characters
                // which should be invalid when, say, naming a
                // variable which is what is happening here.
                //
                // Hard codes the variables to snake case but
                // could prolly be changed to use a config
                // value instead in the future :D
                let c = match c {
                    ' ' => '_',
                    _ => c,
                };

                self.variable_popup.text_buffer.push(c);
                self.cursor_handler.incr_x(1);
            }
        }
    }

    /// Inserts a new line at cursors position,
    /// automatically adjusts the rest of the line.
    ///
    /// Used more for a all round handler of `\n` in different popups
    pub fn insert_new_line(&mut self) {
        match &mut self.current_popup {
            EditorPopup::None => {
                let is_splitting_line = self.cursor_handler.x()
                    < self.content[self.cursor_handler.y()].len() + self.get_left_offset();
                let rest = if is_splitting_line {
                    // we have some text we want to move one line down
                    // so we get the rest of it here
                    let offset = self.cursor_handler.x() - self.get_left_offset();
                    self.content[self.cursor_handler.y()].split_off(offset)
                } else {
                    String::new()
                };

                // create a new line
                self.move_cursor(BraideDirection::Down);

                // put the rest of the splitted line on the new line
                self.content.insert(self.cursor_handler.y(), rest);

                // update cursor
                self.cursor_handler.set_x(self.get_left_offset());
            }
            EditorPopup::Variables => {
                let mut variable_found = false;
                for v in &self
                    .variable_popup
                    .variables
                    .search(self.variable_popup.text_buffer.clone())
                {
                    if v.name == self.variable_popup.text_buffer {
                        variable_found = true;
                        break;
                    }
                }

                let results = self.variable_popup.variables.search(self.variable_popup.text_buffer.clone());

                if (self.variable_popup.variables.state.selected().is_some() && results.len() > 0) || variable_found {
                    let index = self.variable_popup.variables.state.selected().unwrap_or(0);
                    // In this case we probable have
                    // selected a variable to read from / insert

                    // setup some variables
                    // BUG: This definetly doesnt work when it comes to loops of < and > FUCK
                    //          if this fucks up as spectactularely as i expect we can probably use
                    //          save states and absolute positioning instead
                    let rel_pointer_position = self.editor_friendly_content().matches('>').count()
                        - self.editor_friendly_content().matches('>').count();
                    let t = &self.variable_popup.text_buffer;
                    let variable = &self.variable_popup.variables.search(t.to_string())[index];

                    // go to line below current one
                    self.cursor_handler.set_x(0);
                    self.cursor_handler.incr_y(1);

                    // Create place to store the strings we want to print.
                    // Vec with capacity hella fast (hopefully)
                    let mut string_storage: Vec<String> = Vec::with_capacity(5);

                    // adds the first comment line
                    string_storage.push(format!(
                        "// goes to variable: {} @ {}",
                        variable.name.to_owned(),
                        variable.offset_from_zero
                    ));

                    // Push the line of chevrons
                    string_storage.push(
                        ">".repeat(variable.offset_from_zero as usize - rel_pointer_position),
                    );

                    string_storage.push(
                        "".to_string()
                    );

                    // Push the other line of chevrons
                    string_storage.push(
                        "<".repeat(variable.offset_from_zero as usize - rel_pointer_position),
                    );

                    // Add line describing what variable we are coming home from
                    string_storage.push(format!(
                        "// goes from variable: {} @ {}",
                        variable.name.to_owned(),
                        variable.offset_from_zero
                    ));

                    // add the strings to the content

                    for string in string_storage.iter().rev() {
                        // pushes the string into the content buffer
                        self.content.insert(self.cursor_handler.get(0).1, string.to_string());

                        // should create a new line
                        self.cursor_handler.set_x(0);
                        self.cursor_handler.incr_y(1);
                    }

                    // place curosr between the chevron columns and
                    // open a new line for the user to write on.
                    self.cursor_handler.pop();
                    self.cursor_handler.incr_y(2);
                    self.cursor_handler.set_x(self.get_left_offset());
                } else {
                    // if there is no highlighted item we dont have
                    // a variable want to read from... probably...
                    // This means we want to create a new variable
                    // *somewhere* in memory

                    // Store how many variables we have to use as a offset
                    let n = self.variable_popup.variables.items.len();

                    // push the new variable to our variable list at a unoccupied memory position.
                    self.variable_popup.variables.items.push(Variable::new(
                        self.memory_offset + n as u16 + 1,
                        self.variable_popup.text_buffer.clone(),
                    ));
                    self.cursor_handler.pop();
                }

                // exit the variable popup
                self.variable_popup.text_buffer = String::new();
                self.variable_popup.highlighted_item = 0;
                self.current_popup = EditorPopup::None;
            }
        }
    }

    /// Deletes the character at the current cursor position.
    /// The cursor position is determined by `self.cursor_handler.x()` and `self.cursor_handler.y()`.
    /// Will account for deleting entire lines.
    pub fn delete_at_cursor(&mut self) {
        match &self.current_popup {
            // Check if we want to delete a character normally or an entire line
            EditorPopup::None => {
                if self.cursor_handler.x() > self.get_left_offset() {
                    let left_offset = self.get_left_offset();
                    self.content[self.cursor_handler.y()]
                        .remove(self.cursor_handler.x() - left_offset - 1);
                    self.cursor_handler.incr_x(-1);
                } else {

                    let cur_line = &self.content[self.cursor_handler.y()];
                    // We cant delete the 0th line so we return early
                    // OR
                    // We cant interact with a line longer than the screen so we return early
                    if self.cursor_handler.y() == 0 || cur_line.len() > self.screen_width as usize {
                        return;
                    }

                    // Clone a copy of the content
                    let content = &self.content[self.cursor_handler.y()].clone();
                    let p_content_len = self.content[self.cursor_handler.y() - 1].len();

                    // Update content
                    self.content[self.cursor_handler.y() - 1].push_str(content);
                    self.content.remove(self.cursor_handler.y());

                    // Update cursor
                    self.cursor_handler.incr_y(-1);
                    self.cursor_handler
                        .set_x(p_content_len + self.get_left_offset());
                }
            }
            EditorPopup::Variables => {
                if self.cursor_handler.x() > 0 {
                    self.variable_popup
                        .text_buffer
                        .remove(self.cursor_handler.x() - 1);
                    self.cursor_handler.incr_x(-1);
                }
            }
        }
    }

    /// Move the cursor in the given direction `dir`.
    pub fn move_cursor(&mut self, dir: BraideDirection) {
        match self.current_popup {
            EditorPopup::None => match dir {
                BraideDirection::Up => {
                    if self.rel_cursor_y >= 1 {
                        self.rel_cursor_y -= 1;
                    } else {
                        if self.scroll >= 1 {
                            self.scroll -= 1;
                            return;
                        }
                    }

                    if self.cursor_handler.y() > 0 {
                        self.cursor_handler.incr_y(-1);
                        if self.cursor_handler.x() > self.content()[self.cursor_handler.y()].len() {
                            self.cursor_handler.set_x(
                                self.content()[self.cursor_handler.y()].len()
                                    + self.get_left_offset(),
                            );
                        }
                    }
                }
                BraideDirection::Left => {
                    // used to be `if self.cursor_handler.x() > 0`, changed to make room for line numbers,
                    // This way we can easily make room for however
                    // many chars the current line number requires
                    if self.cursor_handler.x() > self.get_left_offset() {
                        self.cursor_handler.incr_x(-1);
                    } else if self.cursor_handler.y() != 0 {
                        self.cursor_handler.incr_y(-1);

                        self.cursor_handler.set_x(
                            self.content[self.cursor_handler.y()].len() + self.get_left_offset(),
                        );
                    }
                }
                BraideDirection::Right => {
                    if self.cursor_handler.x() <= self.content[self.cursor_handler.y()].len() + 1 {
                        self.cursor_handler.incr_x(1);
                    } else if self.cursor_handler.y() <= self.content.len()
                        && self.content.len() != self.cursor_handler.y() + 1
                    {
                        self.cursor_handler.set_x(self.get_left_offset());
                        self.cursor_handler.incr_y(1);
                    }
                }
                BraideDirection::Down => {
                    if self.rel_cursor_y <= self.screen_height - 4 {
                        self.rel_cursor_y += 1;
                    } else {
                        self.scroll += 1;
                        return;
                    }

                    if self.cursor_handler.y() + 1 != self.content.len() {
                        self.cursor_handler.incr_y(1);
                        if self.cursor_handler.x() > self.content()[self.cursor_handler.y()].len() {
                            self.cursor_handler.set_x(
                                self.content()[self.cursor_handler.y()].len()
                                    + self.get_left_offset(),
                            );
                        }
                    }
                }
            },
            EditorPopup::Variables => match dir {
                BraideDirection::Up => {
                    if self.variable_popup.variables.len() > 0 {
                        self.variable_popup.variables.previous();
                    }
                }
                BraideDirection::Down => {
                    if self.variable_popup.variables.len() > 0 {
                        self.variable_popup.variables.next();
                    }
                }
                BraideDirection::Left => {
                    if self.cursor_handler.x() != 0 {
                        self.cursor_handler.incr_x(-1);
                    }
                }
                BraideDirection::Right => {
                    if self.cursor_handler.x() < self.variable_popup.text_buffer.len() {
                        self.cursor_handler.incr_x(1);
                    }
                }
            },
        }
    }

    /// Save the currently edited file to disk.
    pub fn save(&mut self) -> Result<(), std::io::Error> {
        let mut file = OpenOptions::new().write(true).truncate(true).open(&self.filename)?;

        let n = file.write(self.content().join("\n").as_bytes())?;
        debug(format!("wrote {n} bytes"));

        Ok(())
    }

    /// Deletes the entire current line in the editor
    pub fn delete_line(&mut self) {
        match self.current_popup {
            EditorPopup::None => {
                if self.content.get(self.cursor_handler.y() + 1).is_some() {
                    self.content.remove(self.cursor_handler.y());
                }
            },
            EditorPopup::Variables =>  {}
        }
    }
}
