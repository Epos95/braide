use tui::widgets::ListState;

pub struct VariablePopup {
    pub text_buffer: String,
    pub variables: VariableList,
    pub highlighted_item: usize,
}

pub struct VariableList {
    pub state: ListState,
    pub items: Vec<Variable>,
}

impl VariableList {
    pub fn len(&self) -> usize {
        self.items.len()
    }

    pub fn with_items(items: Vec<Variable>) -> VariableList {
        VariableList {
            state: ListState::default(),
            items,
        }
    }

    pub fn next(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i >= self.items.len() - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    pub fn previous(&mut self) {
        match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.state.select(None)
                } else {
                    self.state.select(Some(i - 1));
                }
            }
            None => self.state.select(None),
        };
    }

    /// Should return all variables matching `query`.
    pub fn search(&self, query: String) -> Vec<&Variable> {
        self.items
            .iter()
            .filter(|x| x.name.contains(&query))
            .collect::<Vec<&Variable>>()
    }
}

#[derive(Debug, Clone)]
/// Structure describing a brainfuck/braide variable.
pub struct Variable {
    pub offset_from_zero: u16,
    pub name: String,
}

impl Variable {
    pub fn new(offset_from_zero: u16, name: String) -> Self {
        Variable {
            offset_from_zero,
            name,
        }
    }
}

#[cfg(test)]
mod variable_tests {
    use super::*;

    #[test]
    fn search_in_variablelist_one() {
        let mut vl = VariableList::with_items(vec![]);
        vl.items
            .push(Variable::new(1, String::from("variable_one")));
        vl.items
            .push(Variable::new(2, String::from("variable_two")));
        vl.items
            .push(Variable::new(3, String::from("variable_three")));

        assert_eq!("variable_one", vl.search("variable".to_string())[0].name);
        assert_eq!(
            "variable_one",
            vl.search("variable_one".to_string())[0].name
        );
    }

    #[test]
    fn search_in_variablelist_two() {
        let mut vl = VariableList::with_items(vec![]);
        vl.items
            .push(Variable::new(1, String::from("variable_one")));
        vl.items
            .push(Variable::new(2, String::from("variable_test")));
        vl.items
            .push(Variable::new(3, String::from("testing_variable")));

        let r: Vec<String> = vl
            .search("test".to_string())
            .iter()
            .map(|x| x.name.clone())
            .collect();
        assert!(r.contains(&"variable_test".to_string()));
        assert!(r.contains(&"testing_variable".to_string()));
    }

    #[test]
    fn search_in_variablelist_three() {
        let vl = VariableList::with_items(vec![]);

        let r: Vec<String> = vl
            .search("test".to_string())
            .iter()
            .map(|x| x.name.clone())
            .collect();
        assert!(!r.contains(&"variable_test".to_string()));
        assert!(!r.contains(&"testing_variable".to_string()));
    }
}
