use braide::debug;
use braide::BraideMessage;
use braide::InterpretationResult;
use std::sync::mpsc::{Sender, Receiver};

/// A struct defining a interpretation of a specific brainfuck program,
/// this interpretation can then be executed or "stepped through".
#[derive(Clone)]
pub struct Interpretation {
    /// A vector of all the (valid brainfuck) chars in the program.
    pub instructions: Vec<char>,

    /// A vector of all the memory cells, `u8` as according to the brainfuck specification.
    pub memory: Vec<u8>,

    /// The index of the tape pointer into `memory`.
    pub pointer: usize,

    /// The current index into the instructions.
    instruction_index: usize,

    /// The programs output as a `String`.
    output: String,

    /// The current pointers back in the code.
    /// Used for implementing brainfuck loops.
    backtrack_pointers: Vec<usize>,
}

impl Default for Interpretation {
    /// Creates a interpretation of a brainfuck program according to the specification of brainfuck on wikipedia.
    fn default() -> Self {
        Interpretation {
            instructions: vec![],
            memory: vec![0; 30000],
            pointer: 0,
            instruction_index: 0,
            output: String::new(),
            backtrack_pointers: vec![],
        }
    }
}

impl Interpretation {
    /// Creates a `Interpretation` from a `String`, with the default settings.
    pub fn from(input: String) -> Self {
        Interpretation {
            instructions: input
                .chars()
                // filter out nops (alphabetic characters)
                .filter(|x| matches!(x, '+' | '-' | '<' | '>' | '[' | ']' | '.' | ','))
                .collect(),
            ..Default::default()
        }
    }

    /// Sets the index pointed to by `self.pointer` to `v`
    pub fn set_memory(&mut self, v: u8) {
        self.memory[self.pointer] = v;
    }

    /// Resets the `Interpretation` to be re-run.
    pub fn reset(&mut self) {
        self.memory = vec![0; 30000];
        self.pointer = 0;
        self.instruction_index = 0;
        self.output = String::new();
        self.backtrack_pointers = vec![];
    }

    pub fn execute(&mut self, t_tx: Sender<BraideMessage>, t_rx: Receiver<BraideMessage>, iteration_limit: u64) -> (Self, InterpretationResult) {
        let mut iterations = 0;
        while self.instruction_index < self.instructions.len() {
            match self.instructions[self.instruction_index] {
                '+' => self.memory[self.pointer] = self.memory[self.pointer].wrapping_add(1),
                '-' => self.memory[self.pointer] = self.memory[self.pointer].wrapping_sub(1),
                '>' => {
                    self.pointer = self.pointer.wrapping_add(1);

                    if self.pointer > self.memory.len() {
                        self.pointer = self.memory.len() - 1;
                    }
                }
                '<' => {
                    self.pointer = self.pointer.wrapping_sub(1);

                    if self.pointer > self.memory.len() {
                        self.pointer = self.memory.len() - 1;
                    }
                }
                '.' => self.output = format!("{}{}", self.output, self.memory[self.pointer]),
                ',' => {

                    // Tell main thread we need to read input
                    t_tx.send(BraideMessage::NeedInput).unwrap();

                    // Wait for input from the main thread
                    let msg = t_rx.recv().unwrap();

                    // handle the received message by
                    // 1. putting the input into memory
                    // or
                    // 2. panicing since ANY other message is invalid in this state.
                    match msg {
                        BraideMessage::Input(c) => {
                            self.set_memory(c as u8);
                        },
                        _ => { panic!("executing thread received a invalid message.") }
                    }
                }
                '[' => {
                    // Push the backtrack pointer (location of the opening bracket)
                    // to the strack so we can jump back to it later and thereby
                    // loop the program.
                    self.backtrack_pointers.push(self.instruction_index);
                }
                ']' => {
                    // Checks the currently pointed to cell (of memory) and check
                    // if the program should loop back to the latest backtrack
                    // pointer or continue with the program.
                    if self.memory[self.pointer] > 0 {
                        // try and go to the last backtrack pointer.
                        match self.backtrack_pointers.last() {
                            Some(x) => {
                                iterations += 1;
                                self.instruction_index = *x;
                            },

                            // No matching bracket, therefore error.
                            None => return (self.clone(), InterpretationResult::SyntaxError),
                        }
                    } else if self.backtrack_pointers.pop().is_none() {
                        // No backtrack pointer found.
                        return (self.clone(), InterpretationResult::SyntaxError);
                    } else {
                        iterations = 0;
                    }
                }
                _ => {}
            }

            // Go to the next instruction.
            self.instruction_index += 1;

            if iterations > iteration_limit {
                return (self.clone(), InterpretationResult::InfiniteLoop);
            }
        }

        t_tx.send(BraideMessage::Finished).unwrap();

        // Succesfully executed all the code!
        (self.clone(), InterpretationResult::Success)
    }

    /// Dumps the current state of the program interpretation
    /// in human readable format.
    pub fn dump_memory(&self) -> String {
        let mut res = String::new();

        // Print the non-zero values of all memory cells along with their indices.
        for (index, cell) in self.memory.iter().enumerate() {
            if *cell != 0 {
                res = format!("{}{} @ {}\n", res, cell, index);
            }
        }

        if res.is_empty() {
            res = String::from("All cells are 0 / empty!");
        }

        res
    }

    /// Dumps the instructions of the current program as a `String`.
    pub fn dump_instructions(&self) -> String {
        self.instructions.iter().collect::<String>()
    }

    /// Dump the output of the program.
    pub fn dump_output(&self) -> String {
        self.output.clone()
    }
}
