use std::fs::OpenOptions;
use std::io::Write;

use tui::{layout::Direction as TDirection, layout::*};

/// Function for debugging a string to `LOG.txt`.
pub fn debug<S: AsRef<str>>(s: S)
where
    S: std::fmt::Display,
{
    let s = format!("{}\n", s);
    OpenOptions::new()
        .append(true)
        .write(true)
        .create(true)
        .open("LOG.txt")
        .unwrap()
        .write_all(s.as_bytes())
        .unwrap();
}

/// Enum representing a message to be passed between the thread executing the `Interpretation`
/// and the main UI thread.
/// Is made to be sent over the `std::sync::mpsc::Channel`.
#[derive(Copy, Clone, Debug)]
pub enum BraideMessage {
    /// The exeucting thread needs to read a bit of input, signals to main thread that it
    /// needs some input.
    NeedInput,

    /// The data returned from the main thread, `char` since brainfuck reads characters one
    /// character at a time to then be converted to a u8.
    Input(char),

    /// The executing thread is done so the main thread can exit the loop.
    Finished,
}

unsafe impl Send for BraideMessage {}
unsafe impl Sync for BraideMessage {}

/// Represent a result from a `Interpretation::execute()`.
/// Execution of brainfuck code can only result in a success or a syntax error from
/// unmatched `[` and `]`.
///
/// If the brainfuck code which is executing requires input, it gets handled by sending
/// `BraideMessage`'s between the UI / main thread and the executing thread.
#[derive(Copy, Clone, Debug)]
pub enum InterpretationResult {
    /// Sucessfully executed all code.
    Success,

    /// Error in syntax e.g unmatched brackets.
    SyntaxError,

    /// A infinite loop was detected in the code
    InfiniteLoop,
}

/// Enum for moving in a certain direction
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

/// helper function to create a centered rect using up certain percentage of the available rect `r`
///
/// Used for the popups :D
pub fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(TDirection::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - percent_y) / 2),
                Constraint::Percentage(percent_y),
                Constraint::Percentage((100 - percent_y) / 2),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(TDirection::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - percent_x) / 2),
                Constraint::Percentage(percent_x),
                Constraint::Percentage((100 - percent_x) / 2),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}
