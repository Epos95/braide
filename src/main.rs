pub mod app;
pub mod editor;
pub mod interpreter;
pub mod open_tab;

use core::time::Duration;
use braide::{BraideMessage, InterpretationResult};
pub use app::App;
use braide::debug;
pub use editor::editor::{Editor, EditorPopup};
pub use interpreter::Interpretation;
use open_tab::OpenTab;
use std::sync::mpsc::{Sender, Receiver, RecvTimeoutError};
use std::sync::mpsc;

use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyModifiers},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use std::{
    thread,
    env,
    io::{self, ErrorKind},
};
use tui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};

fn main() {
    // Get file name from args or a default filename (test.bf)
    let fname = env::args().nth(1).unwrap_or_else(|| "test.bf".to_string());

    // setup terminal
    enable_raw_mode().unwrap();
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture).unwrap();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend).unwrap();

    // create app and run it
    let app = app::App::new(fname);
    let res = run_app(&mut terminal, app);

    // restore terminal
    disable_raw_mode().unwrap();
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )
    .unwrap();
    terminal.show_cursor().unwrap();

    if let Err(err) = res {
        println!("{:?}", err);
    }
}

fn run_app<B: Backend>(terminal: &mut Terminal<B>, mut app: App) -> io::Result<()> {
    loop {
        terminal.draw(|f| {
            let tab = app.open_tab;
            tab.render(f, &mut app);
        })?;

        // Get the currently pressed key and loop back otherwise
        let key = if let Event::Key(key) = event::read()? {
            key
        } else {
            continue;
        };

        // We can match all kinds of open tabs by just ignoring it in the match statement :D
        match (key.modifiers, key.code, &app.open_tab) {
            // Global Keybindings:

            // Exit application
            (KeyModifiers::CONTROL, KeyCode::Char('c'), _) => {
                return Ok(());
            }

            // Global keybindings:

            // Run the current file and go to running tab
            (KeyModifiers::CONTROL, KeyCode::Char('r'), _) => {
                app.open_tab = OpenTab::Run;
                app.tab_index = 1;

                let mut inter = Interpretation::from(app.editor.content().join(""));

                let (main_tx, thread_rx): (Sender<BraideMessage>, Receiver<BraideMessage>) = mpsc::channel();
                let (thread_tx, main_rx): (Sender<BraideMessage>, Receiver<BraideMessage>) = mpsc::channel();

                let handle = thread::spawn(move || {
                    let r = inter.execute(thread_tx, thread_rx, app.iteration_limit);
                    r
                });


                loop {
                    // Redraw terminal
                    terminal.draw(|f| {
                        let tab = app.open_tab;
                        tab.render(f, &mut app);
                    })?;

                    // Safeguard timeout detection...
                    // in case guards in interpreter.rs doesnt catch the infinite loop
                    let dur = Duration::from_millis(8000);
                    let msg = match main_rx.recv_timeout(dur) {
                        Ok(s) => {s},
                        Err(e) => {
                            match e {
                                RecvTimeoutError::Timeout => {
                                    panic!("Infinite loop detected!")
                                },
                                RecvTimeoutError::Disconnected => {
                                    BraideMessage::Finished
                                }
                            }
                        }
                    };

                    match msg {
                        BraideMessage::Finished => {break},
                        BraideMessage::NeedInput => {
                            app.current_message = "Need input: ".to_string();

                            terminal.draw(|f| {
                                let tab = app.open_tab;
                                tab.render(f, &mut app);
                            })?;

                            if let Event::Key(key) = event::read()? {
                                match key.code {
                                    KeyCode::Char(c) => {
                                        main_tx.send(BraideMessage::Input(c)).unwrap();
                                        app.input_buffer.push(c);
                                    },
                                    _ => { continue }
                                }
                            }

                        },
                        _ => { panic!("Main thread recieved a invalid message.")},
                    }
                }

                let result = handle.join().unwrap();

                match result {
                    (inter, InterpretationResult::Success) => {
                        app.current_message = "Successfully executed the code!".to_string();
                        app.interpreter = inter;
                    },
                    (_, InterpretationResult::SyntaxError) => {
                        app.current_message = "Error: Syntax error!".to_string();
                    },
                    (_, InterpretationResult::InfiniteLoop) => {
                        app.current_message = "Error: Infinite loop detected".to_string();
                    }
                }
            }
            (KeyModifiers::NONE, KeyCode::Char('1'), _) => {
                // reset the input buffer
                app.input_buffer = String::new();

                app.interpreter.reset();
                app.open_tab = OpenTab::Editor;
                app.tab_index = 0;
            }
            (KeyModifiers::NONE, KeyCode::Char('2'), _) => {
                app.open_tab = OpenTab::Run;
                app.tab_index = 1;
            }

            // Editor keybindings:

            // For testing:
            (KeyModifiers::CONTROL, KeyCode::Char('j'), OpenTab::Editor) => {
                debug(format!("screen_height: {}", app.editor.screen_height - 2));
                app.editor.scroll += 1;
            }

            (KeyModifiers::CONTROL, KeyCode::Char('k'), OpenTab::Editor) => {
                if app.editor.scroll > 0 {
                    app.editor.scroll -= 1;
                }
            }

            // END TESTING

            // Save currently edited file
            (KeyModifiers::CONTROL, KeyCode::Char('s'), OpenTab::Editor) => match app.editor.save()
            {
                Ok(_) => {
                    app.current_message = "Succesfully saved file!".to_string();
                    app.interpreter = Interpretation::from(app.editor.editor_friendly_content());
                }
                Err(e) => {
                    app.current_message = match e.kind() {
                        ErrorKind::NotFound => "[ERROR] File not found!".to_string(),
                        ErrorKind::PermissionDenied => {
                            "[ERROR] Permission to write was denied.".to_string()
                        }
                        _ => format!("[ERROR] \"{:?}\"", e.kind()),
                    };
                }
            },

            // This can be written so much better lmao
            (KeyModifiers::NONE, KeyCode::Left, OpenTab::Editor) => {
                app.editor.move_cursor(braide::Direction::Left)
            }
            (KeyModifiers::NONE, KeyCode::Right, OpenTab::Editor) => {
                app.editor.move_cursor(braide::Direction::Right)
            }
            (KeyModifiers::NONE, KeyCode::Down, OpenTab::Editor) => {
                app.editor.move_cursor(braide::Direction::Down)
            }
            (KeyModifiers::NONE, KeyCode::Up, OpenTab::Editor) => {
                app.editor.move_cursor(braide::Direction::Up)
            }

            // Toggle editor variables popup.
            (KeyModifiers::CONTROL, KeyCode::Char('a'), OpenTab::Editor) => {
                match app.editor.current_popup {
                    EditorPopup::None => {
                        app.editor.current_popup = EditorPopup::Variables;
                        app.editor.cursor_handler.add(0, 0);
                    }
                    EditorPopup::Variables { .. } => {
                        app.editor.variable_popup.text_buffer = String::new();
                        app.editor.current_popup = EditorPopup::None;

                        // Pop the last cursor from the cursor array
                        app.editor.cursor_handler.pop();

                        // Reset the selected variable
                        app.editor.variable_popup.variables.state.select(None);
                    }
                }
            }

            (KeyModifiers::CONTROL, KeyCode::Char('d'), OpenTab::Editor) => {
                app.editor.delete_line();
            }

            (KeyModifiers::NONE, KeyCode::Backspace, OpenTab::Editor) => {
                app.editor.delete_at_cursor()
            }
            (KeyModifiers::NONE, KeyCode::Enter, OpenTab::Editor) => app.editor.insert_new_line(),
            (KeyModifiers::NONE, KeyCode::Char(c), OpenTab::Editor) => {
                app.editor.insert_at_cursor(c)
            }
            (KeyModifiers::NONE, KeyCode::Delete, OpenTab::Editor) => {
                app.editor.move_cursor(braide::Direction::Right);
                app.editor.delete_at_cursor();
            }
            _ => {}
        }
    }
}
