use crate::{App, EditorPopup};

use itertools::Itertools;
use tui::{
    backend::Backend,
    layout::{Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, List, ListItem, Paragraph, Tabs, Wrap},
    Frame,
};

/// Enum describing all the possible tabs that we can have opened.
#[derive(Clone, Copy)]
pub enum OpenTab {
    Editor,
    Run,
}

impl OpenTab {
    /// Renders the Apps currently open tab.
    pub fn render<B>(&self, f: &mut Frame<B>, app: &mut App)
    where
        B: Backend,
    {
        match self {
            // Renders editor
            OpenTab::Editor => {
                let size = f.size();
                let chunks = Layout::default()
                    .direction(Direction::Vertical)
                    .margin(1)
                    .constraints(
                        [
                            // Says that the first chunk should be of height/length 3
                            Constraint::Length(3),
                            // Sets the second chunk to be atleast 5 chars high/wide
                            Constraint::Min(5),
                            // Footer for showing messages and commands
                            Constraint::Length(1),
                        ]
                        .as_ref(),
                    )
                    .split(size);

                // aparently this block is the ENTIRE screen
                let block = Block::default().style(Style::default());

                // so here we render the entire screen layout on the terminal
                f.render_widget(block, size);

                // create a vector of spans
                let titles = app
                    .titles
                    .iter()
                    .map(|t| {
                        // spans to allow each tab to be styled independently
                        Spans::from(Span::styled(t, Style::default().fg(Color::Yellow)))
                    })
                    .collect();

                let tabs = Tabs::new(titles)
                    .block(Block::default().borders(Borders::ALL))
                    .select(app.tab_index)
                    .style(Style::default())
                    .highlight_style(
                        Style::default()
                            .add_modifier(Modifier::BOLD)
                            .bg(Color::Black),
                    );
                f.render_widget(tabs, chunks[0]);

                let inner = Paragraph::new(app.editor.editor_friendly_content())
                    .block(Block::default().borders(Borders::ALL))
                    .style(Style::default().add_modifier(Modifier::RAPID_BLINK))
                    .scroll((app.editor.scroll, 0));
                f.render_widget(inner, chunks[1]);

                // for testing scrolling in the editor view
                app.editor.screen_height = chunks[1].height;

                // To disallow users from writing outside the right border
                // of the text buffer window thing.
                app.editor.screen_width = chunks[1].width;

                let footer = Paragraph::new(format!("  {}", app.current_message.clone()));
                f.render_widget(footer, chunks[2]);

                match &app.editor.current_popup {
                    EditorPopup::Variables => {
                        let area = braide::centered_rect(80, 60, size);

                        let chunks = Layout::default()
                            .direction(Direction::Vertical)
                            .margin(1)
                            .constraints([Constraint::Length(2), Constraint::Min(5)].as_ref())
                            .split(area);

                        let block = Block::default().title("Variables").borders(Borders::ALL);

                        let text = &app.editor.variable_popup.text_buffer;
                        let text_buffer = Paragraph::new(text.as_str());

                        // Needs a state in the `Variable` struct to keep track of current index
                        let variables = List::new(
                            app.editor
                                .variable_popup
                                .variables
                                .search(text.to_string())
                                .iter()
                                .map(|x| {
                                    let name = x.name.as_str();
                                    let location = x.offset_from_zero.to_string();
                                    let l = format!(
                                        "{}{}@ {}",
                                        name,
                                        " ".repeat(
                                            area.width as usize - name.len() - location.len() - 4
                                        ),
                                        location
                                    );
                                    ListItem::new(l)
                                })
                                .collect::<Vec<ListItem>>(),
                        )
                        .block(Block::default())
                        .style(Style::default().fg(Color::White))
                        .highlight_style(Style::default().add_modifier(Modifier::ITALIC))
                        // Need to decide wheter to use a highlight symbol or not
                        .highlight_symbol("> ");

                        f.render_widget(tui::widgets::Clear, area); //this clears out the background
                        f.render_widget(block, area);
                        f.render_widget(text_buffer, chunks[0]);
                        f.render_stateful_widget(
                            variables,
                            chunks[1],
                            &mut app.editor.variable_popup.variables.state,
                        );

                        app.cursor(f, chunks);
                    }
                    EditorPopup::None => {
                        // Handle the cursor for the app and the currently open tab
                        app.cursor(f, chunks);
                    }
                }
            }
            // Render run window
            OpenTab::Run => {
                let size = f.size();
                let chunks = Layout::default()
                    .direction(Direction::Vertical)
                    .margin(1)
                    .constraints(
                        [
                            // Says that the first chunk should be of height/length 3
                            Constraint::Length(3),
                            // Sets the second chunk to be atleast 5 chars high/wide
                            Constraint::Min(5),
                            // Footer for showing messages and commands
                            Constraint::Length(1),
                        ]
                        .as_ref(),
                    )
                    .split(size);

                // aparently this block is the ENTIRE screen
                let block = Block::default().style(Style::default());

                // so here we render the entire screen layout on the terminal
                f.render_widget(block, size);

                // create a vector of spans
                let titles = app
                    .titles
                    .iter()
                    .map(|t| {
                        // spans to allow each tab to be styled independently
                        Spans::from(Span::styled(t, Style::default().fg(Color::Yellow)))
                    })
                    .collect();

                let tabs = Tabs::new(titles)
                    .block(Block::default().borders(Borders::ALL))
                    .select(app.tab_index)
                    .style(Style::default())
                    .highlight_style(
                        Style::default()
                            .add_modifier(Modifier::BOLD)
                            .bg(Color::Black),
                    );
                f.render_widget(tabs, chunks[0]);

                let mid_chunks = Layout::default()
                    .direction(Direction::Horizontal)
                    .constraints(
                        [
                            Constraint::Min(10),
                            Constraint::Length(1), // Buffer space
                            Constraint::Length(9),
                        ]
                        .as_ref(),
                    )
                    .split(chunks[1]);

                {
                    // This code does stuff inside of mid_chunks

                    {
                        // This code draws the memory, output, and input sections.
                        // e.g this code draws the rows

                        // The rows we want to draw
                        let things = Layout::default()
                            .direction(Direction::Vertical)
                            .constraints(
                                [
                                    Constraint::Min(2),
                                    Constraint::Length(10),
                                    Constraint::Length(3),
                                ]
                                .as_ref(),
                            )
                            .split(mid_chunks[0]);

                        let memory = Paragraph::new(app.interpreter.dump_memory())
                            .block(Block::default().borders(Borders::ALL).title("Memory"))
                            .style(Style::default().add_modifier(Modifier::RAPID_BLINK));
                        f.render_widget(memory, things[0]);

                        let output = Paragraph::new(app.interpreter.dump_output())
                            .block(Block::default().borders(Borders::ALL).title("Output"))
                            .style(Style::default().add_modifier(Modifier::RAPID_BLINK))
                            .wrap(Wrap { trim: true });

                        f.render_widget(output, things[1]);

                        let input = Paragraph::new(&*app.input_buffer)
                            .block(Block::default().borders(Borders::ALL).title("Input"));
                        f.render_widget(input, things[2]);
                    }

                    let instructions = Paragraph::new(
                        app.interpreter
                            .dump_instructions()
                            .chars()
                            .chunks(5)
                            .into_iter()
                            .map(|chunk| chunk.collect::<String>())
                            .map(|x| format!(" {}\n", x))
                            .collect::<String>(),
                    )
                    .block(
                        Block::default()
                            .borders(Borders::ALL)
                            .border_style(Style::default())
                            .title("INSTR")
                            .title_alignment(tui::layout::Alignment::Center),
                    )
                    .style(Style::default().add_modifier(Modifier::RAPID_BLINK));
                    f.render_widget(instructions, mid_chunks[2]);
                }

                let footer = Paragraph::new(format!("  {}", app.current_message.clone()));
                f.render_widget(footer, chunks[2]);

                // Handle the cursor for the app and the currently open tab
                app.cursor(f, chunks);
            }
        }
    }
}
